docker-up-with-tests:
	@docker-compose up --build -d
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && composer install"
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && php bin/console doctrine:schema:update --force"
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test php bin/console doctrine:database:create || echo 'testing database already exists'"
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test php bin/console doctrine:schema:update --force"
	@make phpunit
	@make behat

docker-up:
	@docker-compose up --build -d
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && composer install"
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && php bin/console doctrine:schema:update --force"
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test php bin/console doctrine:database:create || true "
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test php bin/console doctrine:schema:update --force"
	@echo "You are set and done!"

docker-down:
	@docker-compose down

phpunit:
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test bin/phpunit"

behat:
	@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test vendor/behat/behat/bin/behat"

regenerate-database-test:
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test bin/console doctrine:database:drop --force"
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test bin/console doctrine:database:create"
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=test bin/console doctrine:schema:update --force"

regenerate-database-dev:
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=dev bin/console doctrine:database:drop --force"
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=dev bin/console doctrine:database:create"
		@docker exec vtru-php8 sh -c "cd /var/www/vtru && APP_ENV=dev bin/console doctrine:schema:update --force"