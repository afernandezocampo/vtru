<?php
declare(strict_types=1);


use App\Vtru\Infrastructure\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

class CreateOrderWebTest extends WebTestCase
{
    private OrderRepository $orderRepository;
    private AbstractBrowser $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->orderRepository = $this->client->getContainer()->get(OrderRepository::class);
    }

    public function testOrderCreateWeb(): void
    {
        $this->client->request('POST', '/order');
        $response = $this->client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $body = $response->getContent();
        $decodedJson = json_decode($body, true);
        $this->assertArrayHasKey("id", $decodedJson);
        $this->assertArrayHasKey("total_price", $decodedJson);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->orderRepository->createQueryBuilder("o")
            ->delete()
            ->getQuery()
            ->execute();
    }
}