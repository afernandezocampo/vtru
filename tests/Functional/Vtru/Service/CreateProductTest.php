<?php
declare(strict_types=1);


use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Tests\Fixtures\ProductFixtures;

class CreateProductWebTest extends WebTestCase
{
    private ProductRepository $productRepository;
    private AbstractBrowser $client;
    private ProductFixtures $fixtures;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->productRepository = $this->client->getContainer()->get(ProductRepository::class);
        $factory = $this->client->getContainer()->get(ProductFactory::class);
        $this->fixtures = new ProductFixtures($this->productRepository, $factory);
    }

    public function testHealthCheckWeb(): void
    {
        $this->client->request('GET', '/');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"result":"ok"}', $response->getContent());
    }

    public function testProductCreateWeb(): void
    {
        $this->client->request('POST', '/product', ['name' => 'BurgerChampion', "price" => 50]);
        $response = $this->client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $body = $response->getContent();
        $decodedJson = json_decode($body, true);
        $this->assertArrayHasKey("id", $decodedJson);
        $this->assertArrayHasKey("price", $decodedJson);
        $this->assertArrayHasKey("name", $decodedJson);
    }

    public function testProductCreateFailureWeb(): void
    {
        $this->client->request('POST', '/product', ['name' => '', "price" => 50]);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"error":"The name cannot be empty"}', $response->getContent());
    }

    public function testingProductRepeatedNameWeb(): void
    {
        $this->fixtures->load();
        $this->client->request('POST', '/product', ['name' => 'myRepeatedBurger', "price" => 50]);
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"error":"The product name is already taken"}', $response->getContent());
        $this->fixtures->clean();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->productRepository->createQueryBuilder("p")
            ->delete()
            ->getQuery()
            ->execute();
    }
}