<?php
declare(strict_types=1);

namespace Tests\Functional\Vtru\Service;

use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\HttpFoundation\Response;
use Tests\Fixtures\CreateOrderAndProductFixtures;

class GetOrderTest extends WebTestCase
{
    private ProductRepository $productRepository;
    private OrderRepository $orderRepository;
    private CreateOrderAndProductFixtures $fixtures;
    private AbstractBrowser $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->productRepository = $this->client->getContainer()->get(ProductRepository::class);
        $factory = $this->client->getContainer()->get(ProductFactory::class);
        $this->orderRepository = $this->client->getContainer()->get(OrderRepository::class);
        $orderFactory = $this->client->getContainer()->get(OrderFactory::class);
        $this->fixtures = new CreateOrderAndProductFixtures(
            $this->productRepository,
            $factory,
            $this->orderRepository,
            $orderFactory
        );
    }

    public function testGetOrderServiceWeb(): void
    {
        $this->fixtures->load();
        $order = $this->fixtures->getOrder();
        $this->client->request('GET', '/order/' . $order->getId()->value());
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(
            $this->getExepectedJsonFormatBody($order),
            $response->getContent()
        );
        $this->fixtures->clean();
    }

    private function getExepectedJsonFormatBody(Order $order): string
    {
        $string = '
        {
           "id":%id%,
           "total_price":343,
           "products":[
              {
                 "id":%productOneId%,
                 "quantity":10
              },
              {
                 "id":%productTwoId%,
                 "quantity":2
              }
           ]
        }
    ';
        $string = str_replace([
            "%id%",
            "%productOneId%",
            "%productTwoId%",
        ],
            [
                (string)$order->getId()->value(),
                (string)$order->getOrderLines()->get(0)->getProduct()->getId()->value(),
                (string)$order->getOrderLines()->get(1)->getProduct()->getId()->value(),
            ],
            $string);
        return trim(preg_replace('/\s\s+/', '', $string));
    }

    public function testGetNonExistentOrder(): void
    {
        $this->client->request('GET', '/order/' . 4123871);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertEquals('{"error":"The resource you are requesting does not exist"}', $response->getContent());
    }
}