<?php
declare(strict_types=1);

namespace Tests\Functional\Vtru\Service;

use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\HttpFoundation\Response;
use Tests\Fixtures\CreateOrderAndProductFixtures;

class UpdateOrderTest extends WebTestCase
{
    const QUANTITY_TO_ADDED = 2;
    private ProductRepository $productRepository;
    private OrderRepository $orderRepository;
    private CreateOrderAndProductFixtures $fixtures;
    private AbstractBrowser $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->productRepository = $this->client->getContainer()->get(ProductRepository::class);
        $factory = $this->client->getContainer()->get(ProductFactory::class);
        $this->orderRepository = $this->client->getContainer()->get(OrderRepository::class);
        $orderFactory = $this->client->getContainer()->get(OrderFactory::class);
        $this->fixtures = new CreateOrderAndProductFixtures(
            $this->productRepository,
            $factory,
            $this->orderRepository,
            $orderFactory
        );
    }

    public function testUpdateOrderServiceWeb(): void
    {
        $this->fixtures->load();
        $productToAdd = $this->fixtures->createAdditionalProduct();
        $order = $this->fixtures->getOrder();
        $this->client->request('POST', '/order/product/add', [
            'orderId' => $order->getId()->value(),
            'productId' => $productToAdd->getId()->value(),
            'quantity' => self::QUANTITY_TO_ADDED
        ]);
        $response = $this->client->getResponse();
        $decodedJson = json_decode($response->getContent(), true);
        //Selecting the third element (the last one)
        $lastProductAdded = $decodedJson['products'][2];

        $this->assertEquals($productToAdd->getId()->value(), $lastProductAdded['id']);
        $this->assertEquals(self::QUANTITY_TO_ADDED, $lastProductAdded['quantity']);
        $this->fixtures->clean();
    }

    public function testUpdateOrderServiceNoQuantityWeb(): void
    {
        $this->fixtures->load();
        $productToAdd = $this->fixtures->createAdditionalProduct();
        $order = $this->fixtures->getOrder();
        $this->client->request('POST', '/order/product/add', [
            'orderId' => $order->getId()->value(),
            'productId' => $productToAdd->getId()->value(),
        ]);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"The quantity cannot be zero or empty"}', $this->client->getResponse()->getContent());
        $this->fixtures->clean();
    }

    public function testUpdateOrderServiceOrderDoNotExistWeb(): void
    {
        $this->client->request('POST', '/order/product/add', [
            'productId' => 77,
            'quantity' => self::QUANTITY_TO_ADDED
        ]);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"The order you are requesting does not exist, requested id: 0"}', $this->client->getResponse()->getContent());
    }

    public function testUpdateOrderServiceProductNotProvidedWeb(): void
    {
        $this->fixtures->load();
        $order = $this->fixtures->getOrder();
        $this->client->request('POST', '/order/product/add', [
            'orderId' => $order->getId()->value(),
            'quantity' => self::QUANTITY_TO_ADDED
        ]);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"The product requested does not exist, requested id: 0"}', $this->client->getResponse()->getContent());
        $this->fixtures->clean();
    }
}