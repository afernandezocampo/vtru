<?php

declare(strict_types=1);

namespace Tests\Behat;

use App\Vtru\Domain\Entity\OrderLine;
use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use Behat\Behat\Context\Context;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Tests\Fixtures\CreateOrderAndProductFixtures;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
final class ApiContext implements Context
{
    const QUANTITY_TO_BE_ADDED = 4;
    /** @var KernelInterface */
    private $kernel;

    /** @var Response|null */
    private $response;

    private ProductRepository $productRepository;
    private OrderRepository $orderRepository;
    private CreateOrderAndProductFixtures $fixtures;

    public function __construct(
        KernelInterface $kernel,
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        ProductFactory $productFactory,
        OrderFactory $orderFactory
    )
    {
        $this->kernel = $kernel;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;

        $this->fixtures = new CreateOrderAndProductFixtures(
            $this->productRepository,
            $productFactory,
            $this->orderRepository,
            $orderFactory
        );
    }

    /**
     * @When sending a request to :path
     */
    public function aDemoScenarioSendsARequestTo(string $path): void
    {
        $this->response = $this->kernel->handle(Request::create($path, 'POST'));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived(): void
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }

    /**
     * @When a demo scenario sends a request to :arg1 with name :arg2 and price :arg3
     */
    public function aDemoScenarioSendsARequestToWithNameAndPrice($arg1, $arg2, $arg3)
    {
        $this->response = $this->kernel->handle(Request::create($arg1, 'POST', ['name' => $arg2, "price" => $arg3]));
    }

    /**
     * @Then the response should be received with status :arg1
     */
    public function theResponseShouldBeReceivedWithStatus($arg1)
    {
        if ($this->response->getStatusCode() != $arg1) {
            throw new \RuntimeException("The status code is wrong, expected " . $arg1 . ", actual: " . $this->response->getStatusCode());
        }
    }

    /**
     * @When sending a request to an existing order
     */
    public function sendingARequestToAnExistingOrder()
    {
        $this->fixtures->load();
        $order = $this->fixtures->getOrder();
        $this->response = $this->kernel->handle(Request::create("/order/" . $order->getId()->value(), 'GET'));
    }

    /**
     * @When Adding a product and quantity to an existing order
     */
    public function addingAProductAndQuantityToAnExistingOrder()
    {
        $this->fixtures->load();
        $productToAdd = $this->fixtures->createAdditionalProduct();
        $order = $this->fixtures->getOrder();
        $this->response = $this->kernel->handle(Request::create('/order/product/add', 'POST', [
            'orderId' => $order->getId()->value(),
            'productId' => $productToAdd->getId()->value(),
            'quantity' => self::QUANTITY_TO_BE_ADDED
        ]));

        $decodedJson = json_decode($this->response->getContent(), true);
        $lastProductAdded = $decodedJson['products'][2];
        if ($productToAdd->getId()->value() != $lastProductAdded['id']) {
            throw new \RuntimeException("The product Id does not match the expectation, expected: " . $productToAdd->getId()->value()
                . " actual: " . $lastProductAdded['id']);
        }
        if (self::QUANTITY_TO_BE_ADDED != $lastProductAdded['quantity']) {
            throw new \RuntimeException("The quantity does not match the expectation, expected: " . self::QUANTITY_TO_BE_ADDED
                . " actual: " . $lastProductAdded['quantity']);
        }
    }

    /**
     * @AfterScenario
     */
    public function iCleanProductCreationData()
    {
        $this->orderRepository->createQueryBuilder('l')
            ->delete(OrderLine::class, "l")
            ->getQuery()
            ->execute();
        $this->orderRepository->createQueryBuilder("o")
            ->delete()
            ->getQuery()
            ->execute();
        $this->productRepository->createQueryBuilder("p")
            ->delete()
            ->getQuery()
            ->execute();
    }
}
