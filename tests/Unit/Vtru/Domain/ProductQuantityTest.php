<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ProductQuantityTest extends TestCase
{
    public function testQuantityCannotBeZero(): void
    {
        $this->expectException(DomainException::class);
        new \App\Vtru\Domain\ValueObject\ProductQuantity(0);
    }
}