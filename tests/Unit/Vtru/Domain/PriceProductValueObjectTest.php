<?php
declare(strict_types=1);

use App\Vtru\Domain\ValueObject\ProductPrice;
use PHPUnit\Framework\TestCase;

class PriceProductValueObjectTest extends TestCase
{
    public function priceBadValueProvider(): array
    {
        return [
            'value below zero' => [
                'price' => -100,
            ],
            'value zero' => [
                'price' => 0,
            ],
            'value above 100' => [
                'price' => 150,
            ],
        ];
    }

    /**
     * @dataProvider priceBadValueProvider
     * @param float $price
     */
    public function testValueObjectPriceErrors(float $price): void
    {
        $this->expectException(DomainException::class);
        new ProductPrice($price);
    }

    public function priceGoodValueProvider(): array
    {
        return [
            'value float' => [
                'price' => 1.5,
            ],
            'value max' => [
                'price' => 100,
            ],
        ];
    }

    /**
     * @dataProvider priceGoodValueProvider
     * @param float $price
     */
    public function testValueObjectGoodPrice(float $price): void
    {
        $valueObject = new ProductPrice($price);
        $this->assertEquals($price, $valueObject->value());
    }
}