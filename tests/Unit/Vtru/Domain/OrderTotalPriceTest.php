<?php
declare(strict_types=1);

use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\OrderLine;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\ValueObject\OrderTotalAmount;
use App\Vtru\Domain\ValueObject\ProductId;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use PHPUnit\Framework\TestCase;

class OrderTotalPriceTest extends TestCase
{

    const QUANTITY_PRODUCT_ONE = 3;
    const PRICE_PRODUCT_ONE = 77;
    const QUANTITY_PRODUCT_TWO = 8;
    const PRICE_PRODUCT_TWO = 9.8;

    public function testTotalZeroOrAboveIsCorrect(): void
    {
        $ObjectValue = new OrderTotalAmount();
        $this->assertEquals(0, $ObjectValue->value());
    }

    private function productCreator(string $name, float $price, int $id): Product
    {
        $product = $this->createMock(Product::class);
        $product->expects($this->any())->method("getId")->willReturn(new ProductId($id));
        $product->expects($this->any())->method("getName")->willReturn(new ProductName($name));
        $product->expects($this->any())->method("getPrice")->willReturn(new ProductPrice($price));
        return $product;
    }

    public function testTotalAmountSum(): void
    {
        $order = Order::create();
        $order->addOrderLine(
            new ProductQuantity(self::QUANTITY_PRODUCT_ONE),
            $this->productCreator("Burger", self::PRICE_PRODUCT_ONE, 9)
        );
        $order->addOrderLine(
            new ProductQuantity(self::QUANTITY_PRODUCT_TWO),
            $this->productCreator("Burger 2", self::PRICE_PRODUCT_TWO, 12)
        );

        $this->assertEquals(
            (self::PRICE_PRODUCT_ONE * self::QUANTITY_PRODUCT_ONE) + (self::PRICE_PRODUCT_TWO * self::QUANTITY_PRODUCT_TWO),
            $order->getTotalPrice()->value());
    }
}