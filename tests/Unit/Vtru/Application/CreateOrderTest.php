<?php
declare(strict_types=1);

use App\Vtru\Application\CreateOrder;
use App\Vtru\Application\CreateOrderRequest;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\OrderRepositoryInterface;
use App\Vtru\Domain\ValueObject\OrderId;
use App\Vtru\Domain\ValueObject\OrderTotalAmount;
use PHPUnit\Framework\TestCase;

class CreateOrderTest extends TestCase
{
    private CreateOrder $service;
    private OrderRepositoryInterface $repository;
    private OrderFactory $factory;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(OrderRepositoryInterface::class);
        $this->factory = $this->createMock(OrderFactory::class);
        $this->service = new CreateOrder($this->repository, $this->factory);
    }

    public function testOrderCreate(): void
    {
        $order = $this->createMock(Order::class);
        $order->expects($this->once())->method("getId")->willReturn(new OrderId(20));
        $order->expects($this->once())->method("getTotalPrice")->willReturn(new OrderTotalAmount());

        $this->factory->expects($this->once())->method("create")->willReturn($order);
        $this->repository->expects($this->once())->method("save");
        $response = $this->service->__invoke(new CreateOrderRequest());

        $this->assertEquals([
            'id' => 20,
            'total_price' => 0,
        ], $response->toArray());
    }
}