<?php
declare(strict_types=1);

use App\Vtru\Application\UpdateOrder;
use App\Vtru\Application\UpdateOrderRequest;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\OrderRepositoryInterface;
use App\Vtru\Domain\ProductRepositoryInterface;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use PHPUnit\Framework\TestCase;

class UpdateOrderTest extends TestCase
{
    private UpdateOrder $updateOrderService;
    private ProductRepositoryInterface $productRepository;
    private OrderRepositoryInterface $orderRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->productRepository = $this->createMock(ProductRepositoryInterface::class);
        $this->orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $this->updateOrderService = new UpdateOrder($this->orderRepository, $this->productRepository);
    }

    public function testUpdateProductOrder(): void
    {
        $order = $this->createMock(Order::class);
        $this->orderRepository->expects($this->once())->method("getOrder")->willReturn($order);
        $product = $this->createMock(Product::class);
        $this->productRepository->expects($this->once())->method("getProduct")->willReturn($product);
        $response = $this->updateOrderService->__invoke(
            new UpdateOrderRequest(20, 20, 9)
        );
    }

    public function testUpdateProductOrderExceptionNoProduct(): void
    {
        $order = $this->createMock(Order::class);
        $this->orderRepository->expects($this->once())->method("getOrder")->willReturn($order);
        $product = $this->createMock(Product::class);
        $this->productRepository->expects($this->once())->method("getProduct")->willReturn(null);
        $this->expectException(DomainException::class);
        $this->expectErrorMessage('The product requested does not exist, requested id: 20');
        $response = $this->updateOrderService->__invoke(
            new UpdateOrderRequest(109, 20, 9)
        );
    }

    public function testUpdateProductOrderExceptionNoOrder(): void
    {
        $this->orderRepository->expects($this->once())->method("getOrder")->willReturn(null);
        $this->expectException(DomainException::class);
        $this->expectErrorMessage('The order you are requesting does not exist, requested id: 109');
        $response = $this->updateOrderService->__invoke(
            new UpdateOrderRequest(109, 20, 9)
        );
    }
}