<?php
declare(strict_types=1);

use App\Vtru\Application\GetOrder;
use App\Vtru\Application\GetOrderRequest;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\OrderLine;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\OrderRepositoryInterface;
use App\Vtru\Domain\ValueObject\OrderId;
use App\Vtru\Domain\ValueObject\OrderTotalAmount;
use App\Vtru\Domain\ValueObject\ProductId;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class GetOrderTest extends TestCase
{
    private OrderRepositoryInterface $repository;
    private GetOrder $orderService;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(OrderRepositoryInterface::class);
        $this->orderService = new GetOrder($this->repository);
    }

    public function testGetOrder(): void
    {
        $order = $this->createMock(Order::class);
        $order->expects($this->once())->method("getId")->willReturn(new OrderId(245));
        $order->expects($this->once())->method("getTotalPrice")->willReturn(new OrderTotalAmount());
        $collection = new ArrayCollection();
        $collection->add(
            OrderLine::create(
                new ProductQuantity(3),
                $this->createProductMock(33)
            )
        );
        $collection->add(
            OrderLine::create(
                new ProductQuantity(7),
                $this->createProductMock(88)
            )
        );
        $order->expects($this->once())->method("getOrderLines")->willReturn($collection);
        $this->repository->expects($this->once())->method("getOrder")->willReturn($order);
        $response = $this->orderService->__invoke(new GetOrderRequest(34));
        $result = $response->toArray();
    }

    private function createProductMock(int $id): Product
    {
        $productMock = $this->createMock(Product::class);
        $productMock->expects($this->once())->method("getId")->willReturn(new ProductId($id));
        return $productMock;
    }
}