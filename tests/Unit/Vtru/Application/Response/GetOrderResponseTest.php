<?php
declare(strict_types=1);

use App\Vtru\Application\Response\GetOrderResponse;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\OrderLine;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\ValueObject\OrderId;
use App\Vtru\Domain\ValueObject\OrderTotalAmount;
use App\Vtru\Domain\ValueObject\ProductId;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetOrderResponseTest extends TestCase
{
    public function testGetEmptyOrderResponse(): void
    {
        $order = $this->createOrder();
        $order->expects($this->once())->method("getId")->willReturn(new OrderId(33));
        $order->expects($this->once())->method("getTotalPrice")->willReturn(new OrderTotalAmount());
        $order->expects($this->once())->method("getOrderLines")->willReturn(new ArrayCollection());
        $response = new GetOrderResponse($order);
        $arrayResponse = $response->toArray();
        $this->assertArrayHasKey('id', $arrayResponse);
        $this->assertArrayHasKey('total_price', $arrayResponse);
        $this->assertArrayHasKey('products', $arrayResponse);
    }

    private function createOrder(): MockObject
    {
        return $this->createMock(Order::class);
    }

    public function testGetOrderWithProductResponse(): void
    {
        $order = $this->createOrder();
        $order->expects($this->once())->method("getId")->willReturn(new OrderId(245));
        $order->expects($this->once())->method("getTotalPrice")->willReturn(new OrderTotalAmount());
        $collection = new ArrayCollection();
        $collection->add(
            OrderLine::create(
                new ProductQuantity(3),
                $this->createProductMock(33)
            )
        );
        $collection->add(
            OrderLine::create(
                new ProductQuantity(7),
                $this->createProductMock(88)
            )
        );

        $order->expects($this->once())->method("getOrderLines")->willReturn($collection);
        $response = new GetOrderResponse($order);
        $arrayResponse = $response->toArray();
        $this->assertArrayHasKey('id', $arrayResponse);
        $this->assertArrayHasKey('total_price', $arrayResponse);
        $this->assertArrayHasKey('products', $arrayResponse);

        $this->assertEquals([
            "id" => 245,
            "total_price" => 0,
            "products" => [
                ["id" => 33, "quantity" => 3],
                ["id" => 88, "quantity" => 7],
            ]
        ], $arrayResponse);
    }

    private function createProductMock(int $id): Product
    {
        $productMock = $this->createMock(Product::class);
        $productMock->expects($this->once())->method("getId")->willReturn(new ProductId($id));
        return $productMock;
    }
}