<?php
declare(strict_types=1);

use App\Vtru\Application\CreateProduct;
use App\Vtru\Application\CreateProductRequest;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Domain\ProductRepositoryInterface;
use App\Vtru\Domain\ValueObject\ProductId;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;
use PHPUnit\Framework\TestCase;

class CreateProductTest extends TestCase
{
    private CreateProduct $service;
    private ProductRepositoryInterface $repository;
    private ProductFactory $factory;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(ProductRepositoryInterface::class);
        $this->factory = $this->createMock(ProductFactory::class);
        $this->service = new CreateProduct($this->repository, $this->factory);
    }

    public function testNameIsRepeated(): void
    {
        $product = $this->createMock(Product::class);

        $this->expectException(DomainException::class);
        $this->repository->expects($this->once())->method("getProductByName")->willReturn($product);
        $this->service->__invoke(new CreateProductRequest("Sandwich", 80));
    }

    public function testNameIsEmpty(): void
    {
        $this->expectException(DomainException::class);
        $this->service->__invoke(new CreateProductRequest("", 80));
    }

    public function testCreationSuccess(): void
    {
        $product = $this->createMock(Product::class);
        $product->expects($this->once())->method("getId")->willReturn(new ProductId(12));
        $product->expects($this->once())->method("getName")->willReturn(new ProductName("Burger"));
        $product->expects($this->once())->method("getPrice")->willReturn(new ProductPrice(50));

        $this->factory->expects($this->once())->method("create")->willReturn($product);

        $this->repository->expects($this->once())->method("getProductByName")->willReturn(null);
        $this->repository->expects($this->once())->method("save");
        $response = $this->service->__invoke(new CreateProductRequest("Burger", 50));

        $this->assertEquals([
            'id' => 12,
            'name' => "Burger",
            'price' => 50,
        ], $response->toArray());
    }
}