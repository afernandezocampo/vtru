<?php
declare(strict_types=1);

namespace Tests\Fixtures;

use App\Vtru\Application\CreateProductRequest;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Infrastructure\Repository\ProductRepository;

class ProductFixtures
{
    public const FIXTURE_PRODUCT_NAME = "myRepeatedBurger";
    public const FIXTURE_PRODUCT_PRICE = 50;
    private ProductRepository $repository;
    private ProductFactory $factory;

    public function __construct(ProductRepository $repository, ProductFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function load(): void
    {
        $product = $this->factory->create(new CreateProductRequest(
            "myRepeatedBurger",
            50
        ));
        $this->repository->save($product);
    }

    public function clean(): void
    {
        $this->repository->createQueryBuilder("p")
            ->delete()
            ->getQuery()
            ->execute();
    }
}