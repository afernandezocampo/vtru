<?php
declare(strict_types=1);

namespace Tests\Fixtures;


use App\Vtru\Application\CreateProductRequest;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\OrderLine;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use App\Vtru\Infrastructure\Repository\ProductRepository;

class CreateOrderAndProductFixtures
{
    public const FIXTURE_PRODUCT_NAME = "myRepeatedBurger";
    public const FIXTURE_PRODUCT_PRICE = 50;
    private ProductRepository $productRepository;
    private ProductFactory $productFactory;
    private OrderRepository $orderRepository;
    private OrderFactory $orderFactory;
    private Order $order;

    public function __construct(
        ProductRepository $repository,
        ProductFactory $factory,
        OrderRepository $orderRepository,
        OrderFactory $orderFactory)
    {
        $this->productRepository = $repository;
        $this->productFactory = $factory;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
    }

    public function load(): void
    {
        $burger = $this->productFactory->create(new CreateProductRequest("BurgyBurgy", 33.3));
        $bikini = $this->productFactory->create(new CreateProductRequest("Bikini", 5));
        $this->productRepository->save($burger);
        $this->productRepository->save($bikini);
        $this->order = $this->orderFactory->create();
        $this->order
            ->addOrderLine(new ProductQuantity(10), $burger)
            ->addOrderLine(new ProductQuantity(2), $bikini);
        $this->orderRepository->save($this->order);
    }

    public function createAdditionalProduct(): Product
    {
        $burger = $this->productFactory->create(new CreateProductRequest("JapaneseSushi", 77.7));
        $this->productRepository->save($burger);
        return $burger;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function clean(): void
    {
        $this->orderRepository->createQueryBuilder('l')
            ->delete(OrderLine::class, "l")
            ->getQuery()
            ->execute();
        $this->orderRepository->createQueryBuilder("o")
            ->delete()
            ->getQuery()
            ->execute();
        $this->productRepository->createQueryBuilder("p")
            ->delete()
            ->getQuery()
            ->execute();
    }
}