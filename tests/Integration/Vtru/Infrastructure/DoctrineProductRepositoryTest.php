<?php
declare(strict_types=1);

use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;
use App\Vtru\Infrastructure\Repository\ProductRepository;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DoctrineProductRepositoryTest extends KernelTestCase
{
    private ProductRepository $repository;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function setUp(): void
    {
        self::bootKernel();
        /** @var ProductRepository repository */
        $this->repository = (self::$kernel->getContainer())->get(ProductRepository::class);
    }

    public function testProductRepositorySave(): void
    {
        $product = Product::create(
            new ProductName("Burger"),
            new ProductPrice(10)
        );

        $this->repository->save($product);
        $this->assertIsNumeric($product->getId()->value());
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->repository->createQueryBuilder("p")
            ->delete()
            ->getQuery()
            ->execute();
    }
}