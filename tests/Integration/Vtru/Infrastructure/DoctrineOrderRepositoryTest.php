<?php
declare(strict_types=1);

use App\Vtru\Domain\Entity\Order;
use App\Vtru\Infrastructure\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DoctrineOrderRepositoryTest extends KernelTestCase
{
    private OrderRepository $repository;
    private Order $order;

    public function setUp(): void
    {
        self::bootKernel();
        /** @var OrderRepository repository */
        $this->repository = (self::$kernel->getContainer())->get(OrderRepository::class);
    }

    public function testOrderRepositorySave(): void
    {
        $order = Order::create();

        $this->repository->save($order);
        $this->assertIsNumeric($order->getId()->value());
        $this->order = $order;
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->repository->createQueryBuilder("o")
            ->delete()
            ->getQuery()
            ->execute();
    }
}