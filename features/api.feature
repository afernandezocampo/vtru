Feature:
  Product and order Creation

  Scenario: Checking the product creation works and returns what is supposed to and returns status 201
  We try the same process but it should fail because the name is unique
    When a demo scenario sends a request to "/product" with name "MyBurgy" and price "100"
    Then the response should be received with status "201"
    When a demo scenario sends a request to "/product" with name "MyBurgy" and price "100"
    Then the response should be received with status "400"


  Scenario: Checking the order creation works and returns what is supposed to and returns status 201
    When sending a request to "/order"
    Then the response should be received with status "201"

  @debug
  Scenario: Get existing order
    When sending a request to an existing order
    Then the response should be received with status "200"

  Scenario: Adding a product to an existing order
    When Adding a product and quantity to an existing order
    Then the response should be received with status "200"