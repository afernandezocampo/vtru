<?php
declare(strict_types=1);

namespace App\Shared\ValueObject;

use Doctrine\ORM\Mapping as ORM;

abstract class Identification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer",name="id")
     * @var int
     */
    protected int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }
}