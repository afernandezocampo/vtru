<?php
declare(strict_types=1);

namespace App\Shared\ValueObject;


class Money
{
    protected float $value;

    public function __construct(float $value)
    {
        $this->value = $this->formatFloat($value);
    }

    public function value(): float
    {
        return $this->value = $this->formatFloat($this->value);
    }

    private function formatFloat(float $float)
    {
        return round($float, 2);
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }

    public function add(Money $money): void
    {
        $this->value = $this->value + $money->value;
    }

    public function substract(Money $money): void
    {
        $this->value = $this->value - $money->value;
    }
}