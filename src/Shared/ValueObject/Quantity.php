<?php
declare(strict_types=1);

namespace App\Shared\ValueObject;


abstract class Quantity
{
    protected int $value;

    public function __construct(int $value)
    {
        if ($value == 0) {
            throw new \DomainException("The quantity cannot be zero or empty");
        }
        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }

    public function addQuantity(Quantity $quantity): void
    {
        $this->value = $this->value + $quantity->value();
    }
}