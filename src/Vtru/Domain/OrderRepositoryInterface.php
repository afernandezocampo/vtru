<?php
declare(strict_types=1);

namespace App\Vtru\Domain;

use App\Vtru\Domain\Entity\Order;

interface OrderRepositoryInterface
{
    public function save(Order $order): void;

    public function getOrder(int $orderId): ?Order;
}