<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Exception;

class ResourceNotFound extends \DomainException
{

}