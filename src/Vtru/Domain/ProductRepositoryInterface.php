<?php
declare(strict_types=1);

namespace App\Vtru\Domain;


use App\Vtru\Domain\Entity\Product;

interface ProductRepositoryInterface
{
    public function getProductByName(string $name): ?Product;

    public function save(Product $product): void;

    public function getProduct(int $productId): ?Product;
}