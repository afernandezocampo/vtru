<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Factory;

use App\Vtru\Application\CreateProductRequest;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;

class ProductFactory
{
    public function create(CreateProductRequest $request): Product
    {
        return Product::create(
            new ProductName($request->name()),
            new ProductPrice($request->price())
        );
    }
}