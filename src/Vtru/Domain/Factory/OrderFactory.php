<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Factory;

use App\Vtru\Domain\Entity\Order;

class OrderFactory
{
    public function create(): Order
    {
        return Order::create();
    }
}