<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Entity;


use App\Vtru\Domain\ValueObject\ProductId;
use App\Vtru\Domain\ValueObject\ProductName;
use App\Vtru\Domain\ValueObject\ProductPrice;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;

/**
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="App\Vtru\Infrastructure\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\ProductId", columnPrefix = false)) */
    private ProductId $productId;
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\ProductName", columnPrefix = false)) */
    private ProductName $name;
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\ProductPrice", columnPrefix = false)) */
    private ProductPrice $price;

    private function __construct(ProductName $name, ProductPrice $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    static public function create(ProductName $name, ProductPrice $price)
    {
        return new self($name, $price);
    }

    public function getName(): ProductName
    {
        return $this->name;
    }

    public function getPrice(): ProductPrice
    {
        return $this->price;
    }

    public function getId(): ProductId
    {
        return $this->productId;
    }
}