<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Entity;


use App\Shared\ValueObject\Money;
use App\Vtru\Domain\ValueObject\OrderId;
use App\Vtru\Domain\ValueObject\OrderTotalAmount;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;

/**
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="App\Vtru\Infrastructure\Repository\OrderRepository")
 * @ORM\Table(name="order_customer")
 */
class Order
{
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\OrderId", columnPrefix = false)) */
    private OrderId $orderId;
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\OrderTotalAmount", columnPrefix = false)) */
    private OrderTotalAmount $orderTotalPrice;

    /**
     * @ORM\OneToMany(targetEntity="App\Vtru\Domain\Entity\OrderLine",mappedBy="order", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderLines;

    private function __construct()
    {
        $this->orderTotalPrice = new OrderTotalAmount();
        $this->orderLines = new ArrayCollection();
    }

    static public function create()
    {
        return new self();
    }

    public function getId(): OrderId
    {
        return $this->orderId;
    }

    public function getTotalPrice(): OrderTotalAmount
    {
        return $this->orderTotalPrice;
    }

    private function recomputeTotal(OrderLine $orderLine): void
    {
        /** @var OrderLine $orderLine */
        $this->orderTotalPrice->add(
            new Money($orderLine->getQuantity()->value() * $orderLine->getProduct()->getPrice()->value())
        );
    }

    public function addOrderLine(ProductQuantity $productQuantity, Product $product)
    {
        $orderLine = $this->searchOrderLineInStack($product);
        if (!empty($orderLine)) {
            $orderLine->getQuantity()->addQuantity($productQuantity);
        } else {
            $orderLine = OrderLine::create($productQuantity, $product);
            $orderLine->setOrder($this);
            $this->orderLines[] = $orderLine;
        }

        $this->recomputeTotal($orderLine);
        return $this;
    }

    private function searchOrderLineInStack(Product $product): ?OrderLine
    {
        if (empty($this->orderLines)) {
            return null;
        }
        /** @var OrderLine $orderLine */
        foreach ($this->orderLines->toArray() as $orderLine) {
            if ($orderLine->getProduct()->getId()->value() === $product->getId()->value()) {
                return $orderLine;
            }
        }
        return null;
    }

    public function removeApplication(OrderLine $orderLine)
    {
        $this->orderLines->removeElement($orderLine);
    }

    public function getOrderLines()
    {
        return $this->orderLines;
    }
}