<?php
declare(strict_types=1);

namespace App\Vtru\Domain\Entity;

use App\Vtru\Domain\ValueObject\OrderLineId;
use App\Vtru\Domain\ValueObject\ProductQuantity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;

/**
 * @ORM\Entity()
 * @ORM\Table(name="order_line")
 */
class OrderLine
{
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\OrderLineId", columnPrefix = false)) */
    private OrderLineId $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Vtru\Domain\Entity\Order", inversedBy="orderLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private Order $order;
    /** @Embedded(class = "App\Vtru\Domain\ValueObject\ProductQuantity", columnPrefix = false)) */
    private ProductQuantity $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Vtru\Domain\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private Product $product;

    private function __construct(ProductQuantity $quantity, Product $product)
    {
        $this->quantity = $quantity;
        $this->product = $product;
    }

    public static function create(ProductQuantity $quantity, Product $product): self
    {
        return new self($quantity, $product);
    }

    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getQuantity(): ProductQuantity
    {
        return $this->quantity;
    }
}