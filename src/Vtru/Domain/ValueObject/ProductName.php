<?php
declare(strict_types=1);

namespace App\Vtru\Domain\ValueObject;

use App\Shared\ValueObject\StringValueObject;
use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Embeddable
 */
class ProductName extends StringValueObject
{
    /**
     * @ORM\Column(type="string",name="name",unique=true)
     * @var string $value
     */
    protected string $value;
}