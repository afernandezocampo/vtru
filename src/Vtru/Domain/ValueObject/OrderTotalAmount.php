<?php
declare(strict_types=1);

namespace App\Vtru\Domain\ValueObject;

use App\Shared\ValueObject\Money;
use App\Vtru\Domain\Entity\OrderLine;
use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Embeddable
 */
class OrderTotalAmount extends Money
{
    /**
     * @ORM\Column(type="float",precision=15,scale=2,name="total_amount")
     * @var float $value
     */
    protected float $value;

    public function __construct()
    {
        parent::__construct(0);
    }
}