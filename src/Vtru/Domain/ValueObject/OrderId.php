<?php
declare(strict_types=1);

namespace App\Vtru\Domain\ValueObject;

use App\Shared\ValueObject\Identification;
use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Embeddable
 */
class OrderId extends Identification
{

}