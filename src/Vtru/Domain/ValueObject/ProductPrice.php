<?php
declare(strict_types=1);

namespace App\Vtru\Domain\ValueObject;

use App\Shared\ValueObject\Money;

use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Embeddable
 */
class ProductPrice extends Money
{
    /**
     * @ORM\Column(type="float",precision=15,scale=2,name="price")
     * @var float $value
     */
    protected float $value;

    public function __construct(float $value)
    {
        if ($value <= 0 || $value > 100) {
            throw new \DomainException("The price cannot be negative, equal to zero or superior to 100");
        }

        parent::__construct($value);
    }
}