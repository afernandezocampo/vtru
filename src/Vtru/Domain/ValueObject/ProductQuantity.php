<?php
declare(strict_types=1);

namespace App\Vtru\Domain\ValueObject;

use App\Shared\ValueObject\Quantity;
use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Embeddable
 */
class ProductQuantity extends Quantity
{
    /**
     * @ORM\Column(type="integer",name="quantity")
     * @var integer $value
     */
    protected int $value;

}