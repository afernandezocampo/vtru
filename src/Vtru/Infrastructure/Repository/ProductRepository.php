<?php
declare(strict_types=1);

namespace App\Vtru\Infrastructure\Repository;

use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\ProductRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getProductByName(string $name): ?Product
    {
        return $this->findOneBy(["name.value" => $name]);
    }

    public function save(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    public function remove(Product $product): void
    {
        $this->_em->remove($product);
    }


    public function getProduct(int $productId): ?Product
    {
        return $this->findOneBy(["productId.value" => $productId]);
    }
}