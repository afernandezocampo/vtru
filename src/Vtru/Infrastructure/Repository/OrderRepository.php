<?php
declare(strict_types=1);

namespace App\Vtru\Infrastructure\Repository;

use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\OrderRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function save(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function getOrder(int $orderId): ?Order
    {
        return $this->findOneBy(["orderId.value" => $orderId]);
    }

    public function remove(Order $product): void
    {
        $this->_em->remove($product);
    }
}