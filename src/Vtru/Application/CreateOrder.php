<?php
declare(strict_types=1);

namespace App\Vtru\Application;

use App\Vtru\Application\Response\CreateOrderResponse;
use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Factory\OrderFactory;
use App\Vtru\Domain\OrderRepositoryInterface;

class CreateOrder
{
    private OrderRepositoryInterface $orderRepository;
    private OrderFactory $factory;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderFactory $factory
    )
    {
        $this->orderRepository = $orderRepository;
        $this->factory = $factory;
    }

    public function __invoke(CreateOrderRequest $request): CreateOrderResponse
    {
        $order = $this->factory->create();
        $this->orderRepository->save($order);

        return new CreateOrderResponse($order);
    }
}