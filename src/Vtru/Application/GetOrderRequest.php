<?php
declare(strict_types=1);

namespace App\Vtru\Application;


class GetOrderRequest
{
    private int $orderId;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function orderId()
    {
        return $this->orderId;
    }
}