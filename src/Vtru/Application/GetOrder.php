<?php
declare(strict_types=1);

namespace App\Vtru\Application;


use App\Vtru\Application\Response\GetOrderResponse;
use App\Vtru\Domain\Exception\ResourceNotFound;
use App\Vtru\Domain\OrderRepositoryInterface;

class GetOrder
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->orderRepository = $repository;
    }

    public function __invoke(GetOrderRequest $request): GetOrderResponse
    {
        $order = $this->orderRepository->getOrder($request->orderId());
        if (empty($order)) {
            throw new ResourceNotFound('The resource you are requesting does not exist');
        }
        return new GetOrderResponse($order);
    }
}