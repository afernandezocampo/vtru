<?php
declare(strict_types=1);

namespace App\Vtru\Application;


use App\Vtru\Application\Response\UpdateOrderResponse;
use App\Vtru\Domain\Exception\ResourceNotFound;
use App\Vtru\Domain\OrderRepositoryInterface;
use App\Vtru\Domain\ProductRepositoryInterface;
use App\Vtru\Domain\ValueObject\ProductQuantity;

class UpdateOrder
{
    private OrderRepositoryInterface $orderRepository;
    private ProductRepositoryInterface $productRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    public function __invoke(UpdateOrderRequest $request): UpdateOrderResponse
    {
        $order = $this->orderRepository->getOrder($request->orderId());
        if (empty($order)) {
            throw new ResourceNotFound('The order you are requesting does not exist, requested id: ' . $request->orderId());
        }
        $product = $this->productRepository->getProduct($request->productId());
        if (empty($product)) {
            throw new ResourceNotFound('The product requested does not exist, requested id: ' . $request->productId());
        }
        $order->addOrderLine(new ProductQuantity($request->quantity()), $product);
        $this->orderRepository->save($order);

        return new UpdateOrderResponse($order);
    }
}