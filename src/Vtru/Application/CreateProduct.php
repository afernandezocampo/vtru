<?php
declare(strict_types=1);

namespace App\Vtru\Application;

use App\Vtru\Application\Response\CreateProductResponse;
use App\Vtru\Domain\Entity\Product;
use App\Vtru\Domain\Factory\ProductFactory;
use App\Vtru\Domain\ProductRepositoryInterface;
use GuzzleHttp\Promise\Create;

class CreateProduct
{
    private ProductRepositoryInterface $productRepository;
    private ProductFactory $factory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ProductFactory $factory
    )
    {
        $this->productRepository = $productRepository;
        $this->factory = $factory;
    }

    public function __invoke(CreateProductRequest $request): CreateProductResponse
    {
        $this->controlRequest($request);
        $product = $this->factory->create($request);
        $this->productRepository->save($product);

        return new CreateProductResponse($product);
    }

    private function controlRequest(CreateProductRequest $request): void
    {
        if (empty($request->name())) {
            throw new \DomainException("The name cannot be empty");
        }
        if (!empty($this->productRepository->getProductByName($request->name()))) {
            throw new \DomainException("The product name is already taken");
        }
    }
}