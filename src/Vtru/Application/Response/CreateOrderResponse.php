<?php
declare(strict_types=1);

namespace App\Vtru\Application\Response;


use App\Vtru\Domain\Entity\Order;

class CreateOrderResponse
{
    private Order $order;

    public function __construct(Order $product)
    {
        $this->order = $product;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->order->getId()->value(),
            'total_price' => $this->order->getTotalPrice()->value(),
        ];
    }
}