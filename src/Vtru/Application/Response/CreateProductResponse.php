<?php
declare(strict_types=1);

namespace App\Vtru\Application\Response;

use App\Vtru\Domain\Entity\Product;

class CreateProductResponse
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->product->getId()->value(),
            'name' => $this->product->getName()->value(),
            'price' => $this->product->getPrice()->value(),
        ];
    }
}