<?php
declare(strict_types=1);

namespace App\Vtru\Application\Response;

use App\Vtru\Domain\Entity\Order;
use App\Vtru\Domain\Entity\OrderLine;

class GetOrderResponse
{
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function toArray(): array
    {
        $orderLines = $this->formatProductOrderlines($this->order->getOrderLines()->toArray());
        return [
            "id" => $this->order->getId()->value(),
            "total_price" => $this->order->getTotalPrice()->value(),
            "products" => $orderLines
        ];
    }

    /**
     * @param array<OrderLine> $orderLine
     * @return array
     */
    private function formatProductOrderlines(array $orderLine): array
    {
        $result = [];
        /** @var OrderLine $orderline */
        foreach ($orderLine as $orderline) {
            $result[] = [
                'id' => $orderline->getProduct()->getId()->value(),
                'quantity' => $orderline->getQuantity()->value(),
            ];
        }
        return $result;
    }

}