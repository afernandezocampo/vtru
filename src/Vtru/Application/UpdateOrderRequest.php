<?php
declare(strict_types=1);

namespace App\Vtru\Application;


class UpdateOrderRequest
{
    private int $orderId;
    private int $productId;
    private int $quantity;

    public function __construct(int $orderId, int $productId, int $quantity)
    {
        $this->orderId = $orderId;
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    public function orderId(): int
    {
        return $this->orderId;
    }

    public function productId(): int
    {
        return $this->productId;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}