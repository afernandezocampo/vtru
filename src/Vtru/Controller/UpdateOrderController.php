<?php
declare(strict_types=1);

namespace App\Vtru\Controller;


use App\Vtru\Application\UpdateOrder;
use App\Vtru\Application\UpdateOrderRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdateOrderController extends AbstractController
{
    private UpdateOrder $updateService;

    public function __construct(UpdateOrder $updateService)
    {
        $this->updateService = $updateService;
    }

    public function __invoke(Request $request)
    {
        try {
            $serviceResponse = $this->updateService->__invoke(
                new UpdateOrderRequest(
                    (int)$request->request->get("orderId"),
                    (int)$request->request->get("productId"),
                    (int)$request->request->get("quantity"),
                )
            );
            return new JsonResponse($serviceResponse->toArray());
        } catch (\DomainException $e) {
            return new JsonResponse(["error" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}