<?php
declare(strict_types=1);

namespace App\Vtru\Controller;

use App\Vtru\Application\CreateProduct;
use App\Vtru\Application\CreateProductRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateProductController extends AbstractController
{
    private CreateProduct $serviceInvoker;

    public function __construct(CreateProduct $serviceInvoker)
    {
        $this->serviceInvoker = $serviceInvoker;
    }

    public function __invoke(Request $request): JsonResponse
    {
        try {
            $serviceResponse = $this->serviceInvoker->__invoke(
                new CreateProductRequest(
                    (string)$request->request->get("name"),
                    (float)$request->request->get("price")
                )
            );
            return new JsonResponse($serviceResponse->toArray(), Response::HTTP_CREATED);
        } catch (\DomainException $e) {
            return new JsonResponse(["error" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}