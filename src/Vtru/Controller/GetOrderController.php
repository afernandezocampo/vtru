<?php
declare(strict_types=1);

namespace App\Vtru\Controller;


use App\Vtru\Application\GetOrder;
use App\Vtru\Application\GetOrderRequest;
use App\Vtru\Domain\Exception\ResourceNotFound;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetOrderController extends AbstractController
{
    private GetOrder $orderService;

    public function __construct(GetOrder $orderService)
    {
        $this->orderService = $orderService;
    }

    public function __invoke(int $orderId)
    {
        try {
            $response = $this->orderService->__invoke(
                new GetOrderRequest($orderId)
            );
            return new JsonResponse($response->toArray(), Response::HTTP_OK);
        } catch (ResourceNotFound $e) {
            return new JsonResponse(["error" => $e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (\DomainException $e) {
            return new JsonResponse(["error" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}