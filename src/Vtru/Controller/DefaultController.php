<?php
declare(strict_types=1);

namespace App\Vtru\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    public function __invoke(): JsonResponse
    {
        return new JsonResponse(
            [
                "result" => "ok"
            ]
        );
    }
}