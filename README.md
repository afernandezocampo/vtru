# Vtru project

## Quick start
The project is dockerized and therefore for more convience you should have docker installed
along with the `make` linux package to run commands via make.
The root path should be `http://localhost/` and the datbase details are given by the `.env` file. 
Note if you want to access from your mysql local interface you will have to change the host `localhost` or `127.0.0.1` instead of `vtru-mysql`.

* Building the containers and databases and running tests the command is
`make docker-up-with-tests`. If this runs this means all containers and tests are running successfully.

* Turn down the containers `make docker-down`.
* Turn up the containers `make docker-up`.
* Run phpunit tests `make phpunit`.
* Run behat tests `make behat`.

When developing and checking around perhaps it is usefult to regenerate databases:

* To regenerate test database `make regenerate-database-test`.
* To regenerate dev database `make regenerate-database-dev`.

Be aware that in order to run the prior functionalities, these commands are needed because the `.env` and `.env.test`
files have an "inside the container" context.

If you wish to run this outside those containers and you have your own database and server. You will need to change the host in those settings files to point to the `localhost / 127.0.0.1` and set your own password users... etc
. As well as run all the commands manually.

Let's suppose you want to run the phpunit test and assuming you have your database working as long as your server on localhost, inside the project:

* `composer install`
* `APP_ENV=test php bin/console doctrine:database:create`
* `APP_ENV=test php bin/console APP_ENV=test php bin/console doctrine:schema:update --force`
* `APP_ENV=test bin/phpunit`
If you want to run behat:
* `APP_ENV=test vendor/behat/behat/bin/behat`

Docker do simplify things in this case since you won't have to setup your servers manually.


## Special consideration about this project

- This present project is implementing Domain Driven Design in the sense all the application and domain structure are free from infrastructure.
At first look it may seem to verbose and slower to develop upon (Value Objects - doctrine embedded objects- and entities encapsulating logic)
However the scaling capacity tend to be superior.

- Three types of tests are provided: 

    unit, integration and functional tests. Behat tests are also provided eventhough here they are pure functional just to make the point of using them (They are better to work with PO/PM or other non programming parties) and a driver can enhanced its capacities.

- Here we are assuming, when adding products to a command, that if the product already exists in the order, the quantity requested are in addition of the preexisting ones.
For instance, if someone just ordered a bigmac and orders again a bigmac we assume the user wants 2 bigmacs.


